const express = require('express');
const router = express.Router();
const booksController = require('./controllers/booksController');
const authMiddleware = require('../helpers/middlewares/authMiddleware');

// Get all books
router.get('/',  booksController.getAllBooks);

// Get single book
router.get('/:id', booksController.getSingleBook);

// Create new book
router.post('/', booksController.newBook);

// Update book
router.patch('/:id', booksController.updateBook);

// Delete book
router.delete('/:id', booksController.deleteBook);

module.exports = router;