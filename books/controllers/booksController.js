const Book = require('../models/book');

class BooksController {
    static async getAllBooks(req, res) {
        try {
            const books = await Book.find({})
                .sort({createdAt: -1});
            res.json(books)
        } catch (err) {
            console.log('Error occured.')
        }
    }

    static async getSingleBook(req, res) {
        try {
            const book = await Book.findById(req.params.id);
            res.json(book);
        } catch (err) {
            console.log('Error occured.')
        }
    }

    static async newBook(req, res) {
        const book = new Book(req.body);

        try {
            await book.save();
            res.json({msg: 'Book created'})
        } catch (err) {
            console.log('Error occured.')
        }
    }

    static async updateBook(req, res) {
        const obj = {};
        for (let item in req.body) {
            obj[item] = req.body[item]
        }

        try {
            await Book.updateOne({_id: req.params.id}, {$set: obj});
            res.json({msg: 'Book updated'})
        } catch (err) {
            console.log('Error occured.');
        }
    }

    static async deleteBook(req, res) {
        try {
            await Book.deleteOne({_id: req.params.id});
            res.json({msg: 'Book deleted'});
        } catch (err) {
            console.log('Error occured.');
        }
    }
}

module.exports = BooksController;