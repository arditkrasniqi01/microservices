const express = require('express');
const router = express.Router();
const ordersController = require('./controllers/ordersController');
const authMiddleware = require('../helpers/middlewares/authMiddleware');

// Get all orders
router.get('/', ordersController.getAllOrders);

// Get single order
router.get('/:id', ordersController.getSingleOrder);

// Create order
router.post('/', ordersController.newOrder);

// Delete order
router.delete('/:id', authMiddleware, ordersController.newOrder);

// Get single customer orders
router.get('/myOrders/:customerId', authMiddleware, ordersController.customerOrders);

module.exports = router;