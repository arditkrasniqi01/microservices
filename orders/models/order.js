const mongoose = require('mongoose');

const order = new mongoose.Schema({
    customerId: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true
    },
    customerName: {
        type: String,
        required: true
    },
    bookId: {
        type: mongoose.SchemaTypes.ObjectId,
        required: true
    },
    bookTitle: {
        type: String,
        required: true
    },
    initialDate: {
        type: Date,
        required: true
    },
    deliveryDate: {
        type: Date,
        required: true
    }
});

module.exports = mongoose.model('Order', order);