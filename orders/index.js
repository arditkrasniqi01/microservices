const http = require('http');
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const app = express();
const bodyParser = require('body-parser');
const router = require('./router');
const config = require('../helpers/config');
const PORT = 9002;

mongoose.connect(config.ORDERS_SERVICE.DB, {useNewUrlParser: true})
    .then(() => {
        console.log('Connected to order_service db')
    });

app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());
app.use(cors());
app.use('/api/orders', router);

// handle errors for not found router (404)
app.use((req, res, next) => {
    const error = new Error('Requested page not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

const server = http.createServer(app);

server.listen(PORT, () => {
    console.log('Order service on port ' + PORT);
});