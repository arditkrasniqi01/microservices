const mongoose = require('mongoose');
const Order = require('../models/order');
const service = require('../../helpers/service');

class OrdersController {
    static async getAllOrders(req, res) {
        try {
            res.json(await Order.find());
        } catch (err) {
            console.log('Error occured.');
        }
    }

    static async getSingleOrder(req, res) {
        try {
            const order = await Order.findOne({_id: req.params.id});
            if (order) {
                const customer = await service.getCustomerById(order.customerId);
                const book = await service.getBookById(order.bookId);
                const orderObj = {
                    initialDate: order.initialDate,
                    deliveryDate: order.deliveryDate,
                    customer: customer.data.name,
                    book: book.data.title
                };

                return res.json(orderObj);
            }
            res.json({msg: 'Order not found'});
        } catch (err) {
            return res.json({error: err.message});
        }
    }

    static async newOrder(req, res) {
        try {
            const customer = await service.getCustomerById(req.body.customerId, req.headers.authorization);

            const order = new Order({
                customerId: mongoose.Types.ObjectId(req.body.customerId),
                customerName: customer.data.name,
                bookId: mongoose.Types.ObjectId(req.body.bookId),
                bookTitle: req.body.bookTitle,
                initialDate: req.body.initialDate,
                deliveryDate: req.body.deliveryDate
            });

            await order.save();
            res.json({msg: 'Order created'})
        } catch (err) {
            return res.json({error: err.message});
        }
    }

    static async deleteOrder(req, res) {
        try {
            await Order.deleteOne({_id: req.params.id});
            res.json({msg: "Order deleted."});
        } catch (err) {
            return res.json({error: err.message});
        }
    }

    static async customerOrders(req, res) {
        try {
            const orders = await Order.find({customerId: req.params.customerId})
                .sort({initialDate: -1});
            return res.json(orders);
        } catch (err) {
            return res.json({error: err.message});
        }
    }
}

module.exports = OrdersController;