// src/app/auth/token.interceptor.ts
import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor, HttpResponse
} from '@angular/common/http';
import {UserService} from "../components/users/user.service";
import { Observable } from 'rxjs';
import {map, catchError} from "rxjs/operators";
import {Router} from "@angular/router";

@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public auth: UserService, private router: Router) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    request = request.clone({
      setHeaders: {
        Authorization: ''+this.auth.getToken()
      }
    });
    return next.handle(request).pipe(
      catchError(
        (error: any, caught: Observable<HttpEvent<any>>) => {
          if (error.status === 401) {
            this.auth.logout();
            this.router.navigate(['/login']);
          }
          throw error;
        }
      ));
  }
}
