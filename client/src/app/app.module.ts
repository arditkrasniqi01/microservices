import {BrowserModule} from '@angular/platform-browser';
import {FormsModule} from "@angular/forms";
import {NgModule} from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {TokenInterceptor} from "./interceptors/token.interceptor";

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BooksComponent} from './components/books/books.component';
import {LoginComponent} from './components/login/login.component';
import {SignupComponent} from './components/signup/signup.component';
import {OrdersComponent} from './components/orders/orders.component';
import {LogoutComponent} from './components/logout/logout.component';
import { CreateBookComponent } from './components/create-book/create-book.component';

@NgModule({
  declarations: [
    AppComponent,
    BooksComponent,
    LoginComponent,
    SignupComponent,
    OrdersComponent,
    LogoutComponent,
    CreateBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
