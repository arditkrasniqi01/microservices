import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {BooksComponent} from './components/books/books.component';
import {LoginComponent} from "./components/login/login.component";
import {SignupComponent} from "./components/signup/signup.component";
import {OrdersComponent} from "./components/orders/orders.component";
import {LogoutComponent} from "./components/logout/logout.component";
import {CreateBookComponent} from "./components/create-book/create-book.component";
import {AuthGuard} from "./guards/auth.guard";
import {GuestGuard} from "./guards/guest.guard";

const routes: Routes = [{
  path: 'login',
  component: LoginComponent,
  canActivate: [GuestGuard]
},{
  path: 'signup',
  component: SignupComponent,
  canActivate: [GuestGuard]
}, {
  path: '',
  component: BooksComponent
}, {
  path: 'orders',
  component: OrdersComponent,
  canActivate: [AuthGuard]
}, {
  path: 'logout',
  component: LogoutComponent,
  canActivate: [AuthGuard]
}, {
  path: 'create-book',
  component: CreateBookComponent,
  canActivate: [AuthGuard]
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
