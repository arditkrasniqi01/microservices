import {Component} from '@angular/core';
import {UserService} from "./components/users/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'client';
  isAuthenticated: boolean;
  role: string;

  constructor(private userService: UserService, private router: Router) {
    this.router.events.subscribe(val => {
      this.isAuthenticated = this.userService.isAuthenticated();
      this.role = this.userService.getRole();
    });
  }
}
