import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {TokenManagerService} from '../../../../projects/token-manager/src/public_api';
import {Router} from "@angular/router";
import {environment} from "../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {
  }

  isAuthenticated() {
    return TokenManagerService.compareTokens();
  }

  setId(id: string) {
    sessionStorage.setItem('cid', id);
  }

  getId() {
    return sessionStorage.getItem('cid');
  }

  getToken(): any {
    return TokenManagerService.get('token');
  }

  setRole(role: string) {
    sessionStorage.setItem('role', role);
  }

  getRole(): string {
    return sessionStorage.getItem('role');
  }

  signup(user) {
    return this.http.post(`${environment.CUSTOMERS_SERVICE.BASE_URL}`, user);
  }

  login(email, password) {
    return this.http.post(`${environment.CUSTOMERS_SERVICE.BASE_URL}/login`, {email, password});
  }

  setToken(token) {
    TokenManagerService.setToken('Bearer ' + token);
  }

  logout() {
    TokenManagerService.destroy();
    sessionStorage.removeItem('cid');
    sessionStorage.removeItem('role');
  }
}
