import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Order} from "./Order";

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  constructor(private http: HttpClient) {
  }

  getOrders(customerId):Observable<Order[]> {
    return this.http.get<Order[]>(`${environment.ORDERS_SERVICE.BASE_URL}/myOrders/${customerId}`);
  }
}
