import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../users/user.service";
import {OrderService} from "./order.service";
import {Order} from "./Order";

@Component({
  selector: 'app-customer',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  orders: Order[] = [];
  spinner: boolean = false;

  constructor(
    private router: Router,
    private userService: UserService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.spinner = true;
    const customerId = this.userService.getId();
    this.orderService.getOrders(customerId).subscribe(orders => {
      this.spinner = false;
      orders.forEach(order => {
        this.orders = [...this.orders, new Order(order)];
      });
    })
  }

  logout(){
    this.userService.logout();
    this.router.navigate(['/']);
  }
}
