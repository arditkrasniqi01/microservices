export class Order {
  private _id: string;
  private customerId: string;
  private customerName: string;
  private bookId: string;
  private bookTitle: string;
  private initialDate: string;
  private deliveryDate: string;

  constructor(order){
    this._id = order._id;
    this.customerId = order.customerId;
    this.customerName = order.customerName;
    this.bookId = order.bookId;
    this.bookTitle = order.bookTitle;
    this.initialDate = this.formatDate(order.initialDate);
    this.deliveryDate = this.formatDate(order.deliveryDate);
  }

  private formatDate(date){
    const d = new Date(date);
    const dateString = d.toDateString();
    const timeString = d.toTimeString().split(" ")[0];
    return dateString +  " " + timeString;
  }
}
