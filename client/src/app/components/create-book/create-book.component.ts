import {Component, OnInit} from '@angular/core';
import {BookService} from "../books/book.service";
import Swal from "sweetalert2";

@Component({
  selector: 'app-create-book',
  templateUrl: './create-book.component.html',
  styleUrls: ['./create-book.component.scss']
})
export class CreateBookComponent implements OnInit {
  spinner: boolean = false;
  error: string;
  title: string = '';
  genre: string = '';
  author: string = '';
  publisher: string = '';

  constructor(private bookService: BookService) {
  }

  ngOnInit() {
  }

  createBook() {
    this.spinner = true;
    const book = {
      title: this.title,
      genre: this.genre,
      author: this.author,
      publisher: this.publisher
    }

    this.bookService.createBook(book).subscribe(response => {
      if(response.hasOwnProperty('error')){
        return this.error = response['error'];
      }
      this.emptyFields();
      Swal.fire('', response['msg'], 'success');
    }, err => {
      console.log(err);
    }, () => {
      this.spinner = false;
    })
  }

  private emptyFields(){
    this.title = '';
    this.genre = '';
    this.author = '';
    this.publisher = '';
    this.error = '';
  }
}
