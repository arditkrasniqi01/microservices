import {Component, OnInit} from '@angular/core';
import {BookService} from "./book.service";
import {UserService} from "../users/user.service";
import {Router} from "@angular/router";
import {Book} from "./Book";
import Swal from 'sweetalert2';

@Component({
  selector: 'app-books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.scss']
})
export class BooksComponent implements OnInit {
  books: Book[] = [];
  spinner: boolean = false;
  isAuthenticated: boolean;

  constructor(
    private bookServce: BookService,
    private userService: UserService,
    private router: Router
  ) {
    this.isAuthenticated = this.userService.isAuthenticated();
  }

  ngOnInit() {
    this.bookServce.getBooks().subscribe(books => {
      books.forEach(book => {
        this.books = [...this.books, new Book(book)];
      })
    });
  }

  orderBook(bookId, bookTitle) {
    this.spinner = true;
    if (!this.isAuthenticated) {
      return this.router.navigate(['/login']);
    }
    const deliveryDate = new Date();
    deliveryDate.setMonth(deliveryDate.getMonth() + 1);
    const order = {
      bookId,
      bookTitle,
      customerId: this.userService.getId(),
      initialDate: new Date(),
      deliveryDate
    };

    this.bookServce.orderBook(order).subscribe(response => {
      this.spinner = false;
      Swal.fire('', 'Book ordered.', 'success')
    })
  }
}
