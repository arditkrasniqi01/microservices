export class Book {
  _id: string;
  title: string;
  genre: string;
  author: string;
  publisher: string;

  constructor(book: Book){
    this._id = book._id;
    this.title = book.title;
    this.genre = book.genre;
    this.author = book.author;
    this.publisher = book.publisher;
  }

  toString(): string {
    return this.title + " : " + this.genre;
  }
}
