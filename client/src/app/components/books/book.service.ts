import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {Observable} from "rxjs";
import {Book} from "./Book";

@Injectable({
  providedIn: 'root'
})
export class BookService {

  constructor(private http: HttpClient) {
  }

  getBooks(): Observable<Book[]> {
    return this.http.get<Book[]>(`${environment.BOOKS_SERVICE.BASE_URL}/`);
  }

  orderBook(order) {
    return this.http.post(`${environment.ORDERS_SERVICE.BASE_URL}`, order);
  }

  createBook(book) {
    return this.http.post(`${environment.BOOKS_SERVICE.BASE_URL}`, book);
  }
}
