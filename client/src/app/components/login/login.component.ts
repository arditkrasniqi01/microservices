import {Component, OnInit} from '@angular/core';
import {UserService} from "../users/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  spinner: boolean = false;
  error: any = false;
  email: string = '';
  password: string = '';

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  login() {
    this.spinner = true;
    this.userService.login(this.email, this.password).subscribe(response => {
      this.userService.setToken(response['token']);
      this.userService.setId(response['customerId']);
      this.userService.setRole(response['role']);
      this.router.navigate(['orders']);
    }, error => {
      this.error = error['error']['msg'];
      this.spinner = false;
    }, () => {
      this.spinner = false;
    })
  }
}
