import {Component, OnInit} from '@angular/core';
import {UserService} from "../users/user.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  spinner: boolean = false;
  error: any = false;
  name: string = '';
  age: number;
  address: string = '';
  email: string = '';
  password: string = '';

  constructor(private userService: UserService, private router: Router) {
  }

  ngOnInit() {
  }

  signup() {
    this.spinner = true;
    const user = {
      name: this.name,
      age: this.age,
      address: this.address,
      email: this.email,
      password: this.password
    };

    this.userService.signup(user).subscribe((response) => {
      this.spinner = false;
      if(response.hasOwnProperty('error')){
        this.error = response['error'];
        return false;
      }
      return this.router.navigate(['login']);
    });
  }
}
