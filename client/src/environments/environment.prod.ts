export const environment = {
  production: true,
  BOOKS_SERVICE: {
    BASE_URL: 'http://localhost:9000/api/books'
  },
  CUSTOMERS_SERVICE: {
    BASE_URL: 'http://localhost:9001/api/customers'
  },
  ORDERS_SERVICE: {
    BASE_URL: 'http://localhost:9002/api/orders'
  }
};
