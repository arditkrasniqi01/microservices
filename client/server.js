const http = require('http');
const express = require('express');
const app = express();

app.use('/', express.static('dist/client'));

const server = http.createServer(app);

server.listen(8000, () => {
  console.log('Main server on port: ' + 8000);
});
