const Customer = require('../models/customer');
const Role = require('../models/role');
const bcrypt = require('bcrypt');

class CustomerController {
    static async getAllCustomers(req, res) {
        try {
            const customers = await Customer.find({});
            res.json(customers);
        } catch (err) {
            return res.json({error: err.message});
        }
    }

    static async getSingleCustomer(req, res) {
        try {
            const customer = await Customer.findOne({_id: req.params.id});
            res.json(customer);
        } catch (err) {
            return res.json({error: err.message});
        }
    }

    static async newCustomer(req, res) {
        const newCustomer = {
            ...req.body,
            roleId: "5cb1817272c66712ac116422" // user role
        };
        const customer = new Customer(newCustomer);
        bcrypt.hash(req.body.password, 10, async (err, hash) => {
            if (err) {
                res.status(500).json({
                    error: err
                });
            }else{
                try {
                    customer.password = hash;
                    await customer.save();
                    res.json({msg: 'Customer created.'});
                } catch (err) {
                    return res.json({error: err.message});
                }
            }
        });
    }

    static async updateCustomer(req, res) {
        const obj = {};
        for (let item in req.body) {
            obj[item] = req.body[item]
        }

        try {
            await Customer.updateOne({_id: req.params.id}, {$set: obj});
            res.json({msg: 'Customer updated'})
        } catch (err) {
            return res.json({error: err.message});
        }
    }

    static async deleteCustomer(req, res) {
        try {
            await Customer.deleteOne({_id: req.params.id});
            res.json({msg: 'Customer deleted.'})
        } catch (err) {
            return res.json({error: err.message});
        }
    }
}

module.exports = CustomerController;