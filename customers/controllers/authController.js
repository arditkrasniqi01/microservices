const fs = require('fs');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');
const Customer = require('../models/customer');

class AuthController {
    static async login(req, res) {
        const customer = await Customer.findOne({email: req.body.email})
            .populate('roleId', 'role');
        if (!customer) {
            return res.status(401).json({msg: 'Auth failed.'})
        }
        const key = fs.readFileSync(__dirname + '/../../helpers/keys/private.key', 'utf8');

        bcrypt.compare(req.body.password, customer.password, (err, result) => {
            if (result) {
                const token = jwt.sign({
                        email: req.body.email,
                        userId: customer._id
                    },
                    key, {
                        expiresIn: '1h'
                    });
                return res.status(200).json({
                    msg: 'Auth successful.',
                    customerId: customer._id,
                    token: token,
                    role: customer.roleId.role
                });
            }

            res.status(401).json({msg: 'Auth failed.'});
        });
    }
}

module.exports = AuthController;