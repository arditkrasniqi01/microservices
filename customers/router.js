const express = require('express');
const router = express.Router();
const customersController = require('./controllers/customersController');
const authController = require('./controllers/authController');
const authMiddleware = require('../helpers/middlewares/authMiddleware');

// Get all customers
router.get('/', authMiddleware, customersController.getAllCustomers);

// Get single customer
router.get('/:id', authMiddleware, customersController.getSingleCustomer);

// Create customer
router.post('/', customersController.newCustomer);

// Update customer
router.patch('/:id', authMiddleware, customersController.updateCustomer);

// Delete customer
router.delete('/:id', authMiddleware,customersController.deleteCustomer);

// Login
router.post('/login', authController.login)

module.exports = router;