const mongoose = require('mongoose');

const roleSchema = new mongoose.Schema({
    role: {
        type: String,
        required: true
    }
});

exports.module = mongoose.model('Role', roleSchema);