const axios = require('axios');

class Http {
    static post(url, data, token) {
        axios.defaults.headers.common['Authorization'] = token;
        return axios.post(url, data);
    }

    static get(url, token) {
        // axios.defaults.headers.common['Authorization'] = token;
        return axios.get(url, {
            headers: {
                Authorization: token
            }
        });
    }
}

module.exports = Http;