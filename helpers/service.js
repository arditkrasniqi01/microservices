const http = require('./http');
const config = require('./config');

class Service {
    static getCustomerById(id, token){
        return http.get(`${config.CUSTOMERS_SERVICE.BASE_URL}/${id}`, token)
    }

    static getBookById(id, token){
        return http.get(`${config.BOOKS_SERVICE.BASE_URL}/${id}`, token)
    }
}

module.exports = Service;