const fs = require('fs');
const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    if(typeof req.headers.authorization !== 'string'){
        return res.status(401).json({
            message: 'Auth failed.'
        })
    }
    const key = fs.readFileSync(__dirname + '/../keys/private.key', 'utf8');
    const token = req.headers.authorization.split(" ")[1];

    try {
        const decodedToken = jwt.verify(token, key);
        req.decodedToken = decodedToken;
        next();
    } catch (err) {
        return res.status(401).json({
            message: 'Auth failed.'
        })
    }
};