const config = {
    BOOKS_SERVICE: {
        BASE_URL: 'http://localhost:9000/api/books',
        DB: 'mongodb://127.0.0.1:27017/book_service'
    },
    CUSTOMERS_SERVICE: {
        BASE_URL: 'http://localhost:9001/api/customers',
        DB: 'mongodb://127.0.0.1:27017/customer_service'
    },
    ORDERS_SERVICE: {
        BASE_URL: 'http://localhost:9002/api/orders',
        DB: 'mongodb://127.0.0.1:27017/order_service'
    },
};

module.exports = config;